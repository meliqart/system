import React, {useEffect, useState} from 'react';

import ProjectMain from "./rightMain/ProjectMain";
import {Link, useParams, useSearchParams} from "react-router-dom";
import {isString} from "lodash";
import TaskMain from "./rightMain/TaskMain";

function RightMain(props) {


    return (
        <div className='main_right_block'>
            <div className='main_right_content'>
                <Link className='main_project' to='/home'>Projects</Link>
                <Link className='main_task' to='/home/task'>Tasks</Link>
            </div>

<ProjectMain/>
        </div>
    );
}

export default RightMain;