import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {workerDelete, workersCreate, workersList, singleWorker, updateWorker} from "../../store/action/workers";
import CustomModal from "../CustomModal";
import _ from "lodash";
import {useNavigate} from "react-router-dom";
import {AiTwotoneDelete} from "react-icons/ai";
import {BiPencil} from "react-icons/bi";

function LeftMain(props) {
    const [showModal, setShow] = useState(false)
    const [formData, setFormData] = useState({
        name: '',
        surName: ''
    })
    const [errors, setErrors] = useState({})
    const [workerId, setWorkerID] = useState('')
    const navigate = useNavigate()
    const dispatch = useDispatch()
    const workers = useSelector(store => store.workers.data)
    const {employer} = useSelector(store => store.login.data);

    useEffect(() => {
        dispatch(workersList());
    }, [])

    const handleChange = (ev) => {
        setFormData({...formData, [ev.target.name]: ev.target.value})
        setErrors({...errors, [ev.target.name]: ''});
    }

    const handleRegister = (ev) => {
        ev.preventDefault()
        let err = {}
        if (!formData.name) {
            err.name = 'Required field'
        }
        if (!formData.surName) {
            err.surName = 'Required field'
        }
        if (!formData.email) {
            err.email = 'Required field'
        }

        if (formData.email && !(/^[a-zA-Z]+[a-zA-Z0-9._-]+[a-zA-Z0-9]@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/.test(formData.email))) {
            err.email = 'Invalid format'
        }

        if (!_.isEmpty(err)) {
            setErrors(err)
            return;
        }
        dispatch(workersCreate({...formData, employerId: employer?.id}, (err, data) => {

            if (!_.isEmpty(err)) {
                setErrors({...errors, email: err?.email, password: err?.password})
            }
            if (data?.status === 'ok') {
                setShow(false)
                dispatch(workersList())
                navigate('/home')
            }
        }))

    }
    const handleDelete = (ev) => {
        ev.stopPropagation();
        dispatch(workerDelete(ev.currentTarget.id))
    }

    const handleChangeData = (ev) => {
        dispatch(singleWorker(ev.currentTarget.id, (err, data) => {
            if (data) {
                setFormData({name: data.name, surName: data.surName})
            }
        }))
        setWorkerID(ev.currentTarget.id)
        setShow(true)
    }


    const handleUpdate = (ev) => {
        ev.preventDefault();
        let err = {}
        if (!formData.name) {
            err.name = 'Required field'
        }
        if (!formData.surName) {
            err.surName = 'Required field'
        }

        if (!_.isEmpty(err)) {
            setErrors(err)
            return;
        }
        dispatch(updateWorker(formData,workerId, (err, data) => {
            setShow(false)
            navigate('/home')
        }))
    }


    return (
        <div className='left_bar_main'>
            {showModal ?
                <CustomModal workerId={workerId} setErrors={setErrors} setShow={setShow}>
                    <form className='worker_create_form'>
                        <h3>{workerId ? 'Update' : 'Register'}</h3>
                        <label className='worker_create_label' htmlFor="name">Name</label>
                        <input value={formData.name} onChange={ev => handleChange(ev)} required
                               className='worker_create_input' name='name'
                               type="text"
                               placeholder="Name" id="name"/>
                        <span className='worker_create_span'>{errors.name}</span>
                        <label className='worker_create_label' htmlFor="surName">surName</label>
                        <input value={formData.surName} onChange={ev => handleChange(ev)} required
                               className='worker_create_input' name='surName'
                               type="text"
                               placeholder="surName" id="surName"/>
                        <span className='worker_create_span'>{errors.surName}</span>
                        {!workerId ?
                            <>
                                <label className='worker_create_label' htmlFor="username">Email</label>
                                <input onChange={ev => handleChange(ev)} required className='worker_create_input'
                                       name='email'
                                       type="text"
                                       placeholder="Email" id="username"/>
                                <span className='worker_create_span'>{errors.email}</span>
                            </>
                            : null}
                        <button onClick={workerId ? ev => handleUpdate(ev) : ev => handleRegister(ev)}
                                className='worker_create_button'>{workerId ? 'Save' : 'Register'}</button>
                    </form>
                </CustomModal>

                : null}
            {workers.length ?
                <>
                    {workers.map(w => (
                        <div key={w.id} id={w.id} className='workers_block'>
                            <p className='workers_block_text'>
                                <span>{w.name[0] + w.surName[0]}</span>
                                {w.name + ' ' + w.surName}
                            </p>
                            <div id={w.id} onClick={ev => handleDelete(ev)} className='workers_block_icon'>
                                <AiTwotoneDelete
                                    color='white' fontSize={20}/></div>
                            <div id={w.id} onClick={ev => handleChangeData(ev)}
                                 className='workers_block_icon'><BiPencil
                                color='white' fontSize={20}/></div>
                        </div>
                    ))}
                </>
                : null}
            <div className='left_bar_main_button'>
                <button onClick={ev => {
                    setWorkerID('')
                    setFormData({name: '', surName: ''})
                    setShow(true)
                }}>
                    Add Worker
                </button>
            </div>
        </div>
    );
}

export default LeftMain;