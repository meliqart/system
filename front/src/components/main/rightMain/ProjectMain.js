import React, {useEffect, useState} from 'react';
import {Link, useNavigate} from "react-router-dom";
import {RiDeleteBin2Fill} from 'react-icons/ri'
import {BiPencil} from 'react-icons/bi'
import {IoMdAddCircleOutline} from 'react-icons/io';
import {useDispatch, useSelector} from "react-redux";
import {projectList, projectDelete, projectCreate, singleProject, updateProject} from '../../../store/action/projectAction'
import moment from 'moment'
import CustomModal from "../../CustomModal";
import _ from "lodash";

function ProjectMain(props) {
    const dispatch = useDispatch()
    const projects = useSelector(store => store.project.data.projects)
    const [showModal, setShow] = useState(false);
    const navigate = useNavigate()
    const [projectId, setProjectId] = useState("")
    const [formData, setFormData] = useState({
        projectName: '',
        description: ''
    })

    const [errors, setErrors] = useState({})


    useEffect(() => {
        dispatch(projectList())
    }, [])

    const handleChange = (ev) => {
        setFormData({...formData, [ev.target.name]: ev.target.value})
        setErrors({...errors, [ev.target.name]: ''});
    }

    const handleClick = (ev) => {
        setProjectId('')
        setFormData({projectName: '', description: ''})
        setShow(true)
    }

    const handleDelete = (ev) => {
        ev.stopPropagation();
        dispatch(projectDelete(ev.currentTarget.id))
    }

    const handleCreateProject = (ev) => {
        ev.preventDefault()
        let err = {}
        if (!formData.projectName) {
            err.projectName = 'Required field'
        }
        if (!formData.description) {
            err.description = 'Required field'
        }

        if (!_.isEmpty(err)) {
            setErrors(err)
            return;
        }
        dispatch(projectCreate(formData, (err, data) => {
            if (data) {
                setShow(false)

            }
        }))
    }

    const handleChangeData = (ev) => {
        dispatch(singleProject(ev.currentTarget.id, (err, data) => {
            if (data) {
                setFormData({projectName: data.project.projectName, description: data.project.description})
            }
        }))
        setProjectId(ev.currentTarget.id);
        setShow(true)
    }
    const handleUpdate = (ev) => {
        ev.stopPropagation();
        ev.preventDefault();
        let err = {}
        if (!formData.projectName) {
            err.projectName = 'Required field'
        }
        if (!formData.description) {
            err.description = 'Required field'
        }

        if (!_.isEmpty(err)) {
            setErrors(err)
            return;
        }
        dispatch(updateProject(formData, projectId, (err, data) => {
            setShow(false)
            navigate('/home')
        }))
        setShow(false)
    }
    return (
        <>
            {showModal ?
                <CustomModal setErrors={setErrors} setShow={setShow}>
                    <form className='worker_create_form'>
                        <h3>{projectId ? 'Update' : 'Add'}</h3>
                        <label className='worker_create_label' htmlFor="name">Project Name</label>
                        <input value={formData.projectName} onChange={handleChange} required
                               className='worker_create_input' name='projectName'
                               type="text"
                               placeholder="Project Name" id="projectName"/>
                        <span className='worker_create_span'>{errors.projectName}</span>
                        <label className='worker_create_label' htmlFor="surName">Description</label>
                        <textarea value={formData.description} onChange={handleChange} required
                                  className='worker_create_text_area' name='description'
                                  type="text"
                                  placeholder="Description" id="description"/>
                        <span className='worker_create_span'>{errors.description}</span>
                        <button onClick={projectId ? handleUpdate : handleCreateProject}
                                className='worker_create_button'>{projectId ? 'Update' : 'Add'}
                        </button>
                    </form>
                </CustomModal> : null

            }

            <div className="container_task_table">
                <h2>Projects</h2>
                <div onClick={handleClick} className='add-project-div'>
                    Add project
                    <IoMdAddCircleOutline fontSize={20}/>
                </div>
                {projects?.length ?
                    <div>
                        <ul className="responsive-table">
                            <li className="table-header">
                                <div className="col col-1">Project Id</div>
                                <div className="col col-2">Project Name</div>
                                <div className="col col-3">Project Description</div>
                                <div className="col col-4">Created Date</div>
                                <div className="col col-4">Updated Date</div>
                                <div className="col col-5"></div>
                                <div className="col col-5"></div>
                            </li>
                            {projects.map(p => (

                                    <li onClick={()=>{navigate(`/home/task/?projectId=${p.id}`)}}  key={p.id} className="table-row">
                                        <div className="col col-1" data-label="Job Id">{p.id}</div>
                                        <div className="col col-2" data-label="Customer Name">{p.projectName}</div>
                                        <div className="col col-3" data-label="Amount">{p.description}</div>
                                        <div className="col col-4" data-label="Payment Status">
                                            {moment(new Date(p.createdAt)).local().format('YYYY-MM-DD HH:mm:ss')}
                                        </div>
                                        <div className="col col-4" data-label="Payment Status">
                                            {moment(new Date(p.updatedAt)).local().format('YYYY-MM-DD HH:mm:ss')}
                                        </div>
                                        <div id={p.id} onClick={ev => handleDelete(ev)} className="col col-5">
                                            <RiDeleteBin2Fill
                                                fontSize={20}/></div>
                                        <div id={p.id} onClick={handleChangeData} className="col col-5"><BiPencil
                                            fontSize={20}/></div>
                                    </li>

                            ))}
                        </ul>
                    </div> : <div className='no-projects'>No projects</div>}
            </div>

        </>
    );
}

export default ProjectMain;