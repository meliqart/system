import React from 'react';
import {RiLogoutBoxRLine} from 'react-icons/ri'
import {useNavigate} from "react-router-dom";
import LeftHeader from "./header/LeftHeader";
import RightHeader from "./header/RightHeader";

function Header(props) {


    return (
        <div className='header'>
            <LeftHeader/>
            <RightHeader/>
        </div>
    );
}

export default Header;