import React from 'react';

function CustomModal({workerId, setErrors, setShow, children}) {

    return (
        <div className='modal' onClick={(e) => {
            setShow(false);
            setErrors({})
        }}>
            <div className='modal__content' onClick={(e) => e.stopPropagation()}>
                {children}
            </div>

        </div>
    );
}

export default CustomModal;