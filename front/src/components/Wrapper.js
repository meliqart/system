import React, {useEffect} from 'react';
import {useNavigate} from "react-router-dom";

function Wrapper(props) {
    const navigate = useNavigate()
    const token = localStorage.getItem('token')

    useEffect(() => {
        window.history.pushState(null, document.title, window.location.href)
        if (!token) {
            navigate('/')
        }
    }, [])
    return (
        <>
            {props.children}
        </>

    );
}

export default Wrapper;