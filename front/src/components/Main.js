import React from 'react';
import LeftMain from "./main/LeftMain";
import RightMain from "./main/RightMain";

function Main(props) {
    return (
        <div className='main'>
            <LeftMain/>
            <RightMain/>
        </div>
    );
}

export default Main;