import React, {useEffect} from 'react';
import {RiLogoutBoxRLine} from "react-icons/ri";
import {useNavigate} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {employer} from "../../store/action/employer";

function RightHeader(props) {
    const navigate = useNavigate();
    const employerData = useSelector((store) => store.employer.data);
    const dispatch = useDispatch()


    useEffect(() => {
        dispatch(employer())
    }, [])


    const handleLogOut = (ev) => {
        ev.preventDefault()
        localStorage.clear();
        window.location.href = '/'
    }
    return (
        <div className='right_bar_header'>
            {
                employerData?.employer ?
                    <h3>{employerData?.employer?.name[0] + '' + employerData?.employer?.surName[0]}</h3>:null
            }

            <div onClick={ev => handleLogOut(ev)} className='log-out-div'>
                <RiLogoutBoxRLine cursor='pointer' fontSize={30}/>
            </div>
        </div>
    );
}

export default RightHeader;