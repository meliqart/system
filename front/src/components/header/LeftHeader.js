import React from 'react';

function LeftHeader(props) {
    return (
        <div className='left_bar_header'>
            <div className='left_bar_header_image_block'>
                <img className='left_bar_header_image' src="images/header.jpg" alt="photo"/>
            </div>
        </div>
    );
}

export default LeftHeader;