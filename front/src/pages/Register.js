import React, {useState,} from 'react';
import _ from 'lodash'
import {useDispatch} from "react-redux";
import {register} from '../store/action/register'
import {useNavigate} from 'react-router-dom'


function Register(props) {
    const dispatch = useDispatch()
    const navigate = useNavigate()
    const [formData, setFormData] = useState({
        first_name: '',
        last_name: '',
        password: '',
        confirm_password: '',
        email: '',
        phone_number: '',
    })
    const [errors, setErrors] = useState({})


    const handleChange = (ev, key) => {
        _.set(formData, key, ev.target.value)
        setFormData({...formData})
        setErrors({...errors, [key]: ''})
    }

    const handleSubmit = (ev) => {
        ev.preventDefault()
        let err = {};
        for (let i in formData) {
            if (formData[i] === '') {
                err[i] = 'Required Field'
            }

        }
        if (formData.email && !(/^[a-zA-Z]+[a-zA-Z0-9._-]+[a-zA-Z0-9]@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/.test(formData.email))) {
            err.email = 'Invalid Format'
        }
        if (formData.phone_number && (!/^\+?(?:[0-9]●?){8,12}[0-9]$/.test(formData.phone_number))) {
            err.phone_number = 'Invalid Format'
        }
        if (formData.password && (!/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/.test(formData.password))) {
            err.password = 'Password must be 8 characters in length, contain numbers and letters'
        }
        if (formData.password && formData.confirm_password && formData.password !== formData.confirm_password) {

            err.confirm_password = 'Password Mismatch'
        }
        if (!_.isEmpty(err)) {
            setErrors(err)
        } else {
            dispatch(register(formData, (err, data) => {
                if (err){
                    if (err.response.data.errors.status === 'phone'){
                        const error = err.response.data.errors.exist;
                        setErrors({
                            ...errors,
                            phone_number: error
                        })
                    }
                    if (err.response.data.errors.status === 'email'){
                        const error = err.response.data.errors.exist;
                        setErrors({
                            ...errors,
                            email: error
                        })
                    }


                }
                if (data?.status === 'Registered') {
                    navigate('/')
                }
            }))
        }
    }

    return (
        <div className='register_block'>
            <form className='register_form'>
                <h3>Register</h3>
                <div className='register_input_blocks'>
                    <div className='register_single_input'>
                        <label className='login_label' htmlFor="username">First Name</label>
                        <input required className='login_input' onChange={ev => handleChange(ev, 'first_name')}
                               type="text"
                        />
                        <span className='login_span'>{errors.first_name}</span>
                    </div>
                    <div className='register_single_input'>
                        <label className='login_label' htmlFor="password">Last Name</label>
                        <input required className='login_input' onChange={ev => handleChange(ev, 'last_name')}
                               type="text"/>
                        <span className='login_span'>{errors.last_name}</span>
                    </div>
                </div>
                <div className='register_input_blocks'>
                    <div className='register_single_input'>
                        <label className='login_label' htmlFor="username">Email</label>
                        <input required className='login_input' onChange={ev => handleChange(ev, 'email')} type="text"
                        />
                        <span className='login_span'>{errors.email}</span>
                    </div>
                    <div className='register_single_input'>
                        <label className='login_label' htmlFor="password">Phone Number</label>
                        <input required className='login_input' onChange={ev => handleChange(ev, 'phone_number')}
                               type="text"/>
                        <span className='login_span'>{errors.phone_number}</span>
                    </div>
                </div>
                <div className='register_input_blocks'>
                    <div className='register_single_input'>
                        <label className='login_label' htmlFor="username">Password</label>
                        <input required className='login_input' onChange={ev => handleChange(ev, 'password')}
                               type="password"
                        />
                        <span className='login_span'>{errors.password}</span>
                    </div>
                    <div className='register_single_input'>
                        <label className='login_label' htmlFor="password">Confirm Password</label>
                        <input required className='login_input' onChange={ev => handleChange(ev, 'confirm_password')}
                               type="password"/>
                        <span className='login_span'>{errors.confirm_password}</span>
                    </div>
                </div>
                <button onClick={ev => handleSubmit(ev)} className='register_button'>Register</button>
            </form>
        </div>
    );
}

export default Register;