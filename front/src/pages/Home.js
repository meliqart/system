import React from 'react';
import Header from "../components/Header";
import Main from "../components/Main";
import Wrapper from "../components/Wrapper";
import {useSelector} from "react-redux";

function Home(props) {

    return (
        <div>
            <Wrapper>
                <Header/>
                <Main/>
            </Wrapper>
        </div>
    );
}

export default Home;