import React, {useEffect, useState} from 'react';
import {useDispatch} from "react-redux";
import {login} from "../store/action/login";
import _ from 'lodash'
import {useNavigate} from "react-router-dom";

function Login(key, value) {
    const [formData, setFormData] = useState({})
    const [errors, setErrors] = useState({})
    const navigate = useNavigate()
    const dispatch = useDispatch()

    useEffect(() => {
        localStorage.clear()
    }, [])


    const handleChange = (ev) => {
        setFormData({...formData, [ev.target.name]: ev.target.value})
        setErrors({...errors, [ev.target.name]: ''})
    }

    const handleSubmit = (ev) => {
        ev.preventDefault()
        let err = {}

        if (!formData.email) {
            err.email = 'Required field'
        }
        if (!formData.password) {
            err.password = 'Required field'
        }

        if (formData.email && !(/^[a-zA-Z]+[a-zA-Z0-9._-]+[a-zA-Z0-9]@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/.test(formData.email))) {
            err.email = 'Invalid format'
        }

        if (!_.isEmpty(err)) {
            setErrors(err)
            return;
        }

        dispatch(login(formData, (err, data) => {

            if (!_.isEmpty(err)) {
                setErrors({...errors, email: err?.email, password: err?.password})
            }
            if (data?.status === 'ok') {
                localStorage.setItem('token', data.token)
                navigate('/home', {replace: true})
            }
        }))

    }

    return (
        <div className='login_block'>
            <form className='login_form'>
                <h3>Login Here</h3>
                <label className='login_label' htmlFor="username">Username</label>
                <input required className='login_input' name='email' onChange={ev => handleChange(ev)} type="text"
                       placeholder="Email or Phone" id="username"/>
                <span className='login_span'>{errors.email}</span>
                <label className='login_label' htmlFor="password">Password</label>
                <input required className='login_input' name='password' onChange={ev => handleChange(ev)}
                       type="password"
                       placeholder="Password" id="password"/>
                <span className='login_span'>{errors.password}</span>
                <button onClick={ev => handleSubmit(ev)} className='login_button'>Log In</button>
            </form>
        </div>
    );
}

export default Login;