import React from 'react';
import {BrowserRouter, Route, Routes} from "react-router-dom";
import Home from "./pages/Home";
import Login from "./pages/Login";
import Register from "./pages/Register";

function App(props) {

    return (
        <>
            <BrowserRouter>
                <Routes>
                    <Route path='/' element = {<Login/>}/>
                    <Route path='/register' element = {<Register/>}/>
                    <Route path='/home' element = {<Home/>}/>
                    <Route path='/home/:name' element = {<Home/>}/>
                </Routes>
            </BrowserRouter>

        </>
    );
}

export default App;