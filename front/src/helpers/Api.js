import axios from "axios";


const api = axios.create({
    baseURL: 'http://localhost:4000/',
})

api.interceptors.request.use((config) => {
    const token = localStorage.getItem('token');
    if (token) {
        config.headers.Authorization = token
    }
    return config

}, (err) => Promise.reject(err))


class Api {

    static registerEmployer = (data) => {
        return (
            api.post('employer/register', {
                    email: data.email,
                    password: data.password,
                    name: data.first_name,
                    surName: data.last_name,
                    phone: data.phone_number,
                }
            )
        )
    }
    static LoginEmployer = (data) => {
        return (
            api.post('employer/login', {
                    email: data.email,
                    password: data.password,
                },
            )
        )
    }
    static getEmployer = () => {
        return (
            api.get('employer/get/data')
        )
    }

    static workersCreate = (data) => {
        return (
            api.post('workers/create', {
                name: data.name,
                surName: data.surName,
                email: data.email,
            })
        )
    }
    static workerDelete = (id) => {
        return (
            api.delete(`workers/delete/${id}`)
        )
    }

    static getWorker = (id) => {
        return (
            api.get(`workers/single/${id}`)
        )
    }
    static updateWorker = (data,id) => {
        return (
            api.put(`workers/update/${id}`,{
                name: data.name,
                surName: data.surName,
            })
        )
    }


    static workersList = () => {
        return (
            api.get('workers/list')
        )
    }


    static getProjects = () => {
        return (
            api.get('projects/list')
        )
    }

    static projectDelete = (id) => {
        return (
            api.delete(`projects/delete/${id}`)
        )
    }
    static getSingleProject = (id) => {
        return (
            api.get(`projects/single/${id}`)
        )
    }

    static projectCreate = (data) => {
        return (
            api.post(`projects/create`,{
                projectName:data.projectName,
                description:data.description
            })
        )
    }
    static updateProject = (data,id) => {
        return (
            api.put(`projects/update/${id}`,{
                projectName: data.projectName,
                description: data.description,
            })
        )
    }

    static hotelUser = () => {
        return (
            api.get('hotels/user/hotel')
        )
    }
    static  getUserData = () => {
        return (
            api.get('users/get/data')
        )
    }
    static  getDataHotelAll = (page, region) => {
        return (
            api.get(`hotels/getHotels`, {
                params: {
                    page: page,
                    region: region
                }
            })
        )
    }
    static  getHotelDataFool = (id) => {
        return (
            api.get(`hotels/getFoolHotel`, {
                params: {
                    id: id
                }
            })
        )
    }
    static  deleteHotel = (id) => {
        return (
            api.delete(`hotels/delete/${id}`)
        )
    }
    static google = (token) => {
        return api.post('/users/google?accessToken=' + token)
    }
    static getRegisterHotel = (hotelData) => {
        return (
            api.post('hotels/create', {
                ...hotelData,
                location: 'aaa'
            })
        )
    }
    static getHotelUpdate = (hotelData) => {
        return (
            api.put('hotels/update', {
                ...hotelData,
                location: 'aAA'
            })
        )
    }
    static getUpdatePassword = (password) => {
        return (
            api.put('users/update/password', {
                ...password
            })
        )
    }
    static getRoomBooking = (booking) => {
        return (
            api.post('booking/create', {
                ...booking
            })
        )
    }

    static mapData = () => {
        return api.get('/map/get')
    }
    static getYourBooking = () => {
        return api.get('/booking/user-data')
    }

}


export default Api
