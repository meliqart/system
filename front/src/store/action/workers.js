export const WORkERS_LIST_REQUEST = 'WORkERS_LIST_REQUEST'
export const WORkERS_LIST_REQUEST_SUCCESS = 'WORkERS_LIST_REQUEST_SUCCESS'
export const WORkERS_LIST_REQUEST_FAIL = 'WORkERS_LIST_REQUEST_FAIL'

export function workersList() {
    return {
        type: WORkERS_LIST_REQUEST,
    }
}

export const WORkERS_CREATE_REQUEST = 'WORkERS_CREATE_REQUEST'
export const WORkERS_CREATE_REQUEST_SUCCESS = 'WORkERS_CREATE_REQUEST_SUCCESS'
export const WORkERS_CREATE_REQUEST_FAIL = 'WORkERS_CREATE_REQUEST_FAIL'


export function workersCreate(formData, cb) {
    return {
        type: WORkERS_CREATE_REQUEST,
        payload: {
            formData,
            cb
        }
    }
}

export const WORkERS_DELETE_REQUEST = 'WORkERS_DELETE_REQUEST'
export const WORkERS_DELETE_REQUEST_SUCCESS = 'WORkERS_DELETE_REQUEST_SUCCESS'
export const WORkERS_DELETE_REQUEST_FAIL = 'WORkERS_DELETE_REQUEST_FAIL'

export function workerDelete(id) {
    return {
        type: WORkERS_DELETE_REQUEST,
        payload: {
            id
        }
    }
}

export const GET_SINGLE_WORKER_REQUEST = 'GET_SINGLE_WORKER_REQUEST'
export const GET_SINGLE_WORKER_REQUEST_SUCCESS = 'GET_SINGLE_WORKER_REQUEST_SUCCESS'
export const GET_SINGLE_WORKER_REQUEST_FAIL = 'GET_SINGLE_WORKER_REQUEST_FAIL'

export function singleWorker(id, cb) {
    return {
        type: GET_SINGLE_WORKER_REQUEST,
        payload: {
            id,
            cb
        }
    }
}

export const UPDATE_WORKER_REQUEST = 'UPDATE_WORKER_REQUEST'
export const UPDATE_WORKER_REQUEST_SUCCESS = 'UPDATE_WORKER_REQUEST_SUCCESS'
export const UPDATE_WORKER_REQUEST_FAIL = 'UPDATE_WORKER_REQUEST_FAIL'

export function updateWorker(formData, id, cb) {
    return {
        type: UPDATE_WORKER_REQUEST,
        payload: {
            formData,
            id,
            cb
        }
    }
}