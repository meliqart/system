export const REGISTER_REQUEST = 'REGISTER_REQUEST'
export const REGISTER_REQUEST_SUCCESS = 'REGISTER_REQUEST_SUCCESS'
export const REGISTER_REQUEST_FAIL = 'REGISTER_REQUEST_FAIL'

export function register(formData, cb) {
    return {
        type: REGISTER_REQUEST,
        payload: {
            formData,
            cb
        }
    }
}