export const PROJECT_LIST_REQUEST = 'PROJECT_LIST_REQUEST'
export const PROJECT_LIST_REQUEST_SUCCESS = 'PROJECT_LIST_REQUEST_SUCCESS'
export const PROJECT_LIST_REQUEST_FAIL = 'PROJECT_LIST_REQUEST_FAIL'

export function projectList() {
    return {
        type: PROJECT_LIST_REQUEST,
    }
}


export const PROJECT_DELETE_REQUEST = 'PROJECT_DELETE_REQUEST'
export const PROJECT_DELETE_SUCCESS = 'PROJECT_DELETE_SUCCESS'
export const PROJECT_DELETE_FAIL = 'PROJECT_DELETE_FAIL'

export function projectDelete(id) {
    return {
        type: PROJECT_DELETE_REQUEST,
        payload: {
            id
        }
    }
}

export const PROJECT_CREATE_REQUEST = 'PROJECT_CREATE_REQUEST'
export const PROJECT_CREATE_REQUEST_SUCCESS = 'PROJECT_CREATE_REQUEST_SUCCESS'
export const PROJECT_CREATE_REQUEST_FAIL = 'PROJECT_CREATE_REQUEST_FAIL'

export function projectCreate(formData, cb) {
    return {
        type: PROJECT_CREATE_REQUEST,
        payload: {
            formData,
            cb
        }
    }
}

export const GET_SINGLE_PROJECT_REQUEST = 'GET_SINGLE_PROJECT_REQUEST'
export const GET_SINGLE_PROJECT_REQUEST_SUCCESS = 'GET_SINGLE_PROJECT_REQUEST_SUCCESS'
export const GET_SINGLE_PROJECT_REQUEST_FAIL = 'GET_SINGLE_PROJECT_REQUEST_FAIL'

export function singleProject(id, cb) {
    return {
        type: GET_SINGLE_PROJECT_REQUEST,
        payload: {
            id,
            cb
        }
    }
}

export const UPDATE_PROJECT_REQUEST = 'UPDATE_PROJECT_REQUEST'
export const UPDATE_PROJECT_REQUEST_SUCCESS = 'UPDATE_PROJECT_REQUEST_SUCCESS'
export const UPDATE_PROJECT_REQUEST_FAIL = 'UPDATE_PROJECT_REQUEST_FAIL'

export function updateProject(formData, id, cb) {
    return {
        type: UPDATE_PROJECT_REQUEST,
        payload: {
            formData,
            id,
            cb
        }
    }
}
