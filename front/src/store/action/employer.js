export const EMPLOYER_REQUEST = 'EMPLOYER_REQUEST'
export const EMPLOYER_REQUEST_SUCCESS = 'EMPLOYER_REQUEST_SUCCESS'
export const EMPLOYER_REQUEST_FAIL = 'EMPLOYER_REQUEST_FAIL'

export function employer(cb) {
    return {
        type: EMPLOYER_REQUEST,
    }
}