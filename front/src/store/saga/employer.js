import {call, put, takeLatest} from "redux-saga/effects";
import Api from "../../helpers/Api";
import {EMPLOYER_REQUEST, EMPLOYER_REQUEST_SUCCESS, EMPLOYER_REQUEST_FAIL} from "../action/employer";

export default function* watcher() {
    yield takeLatest(EMPLOYER_REQUEST, handleEmployer)
}

function* handleEmployer(action) {
    try {
        const {data} = yield call(Api.getEmployer)

        yield put({
            type: EMPLOYER_REQUEST_SUCCESS,
            payload: {
                data
            }
        })
    } catch (e) {

        yield put({
            type: EMPLOYER_REQUEST_FAIL,
            payload:{
                message: e.message
            }
        })
    }
}