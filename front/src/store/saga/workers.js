import {call, put, takeLatest} from "redux-saga/effects";
import {
    WORkERS_LIST_REQUEST,
    WORkERS_LIST_REQUEST_SUCCESS,
    WORkERS_LIST_REQUEST_FAIL,
    WORkERS_DELETE_REQUEST,
    WORkERS_DELETE_REQUEST_SUCCESS,
    WORkERS_DELETE_REQUEST_FAIL,
    workersList,
    GET_SINGLE_WORKER_REQUEST,
    GET_SINGLE_WORKER_REQUEST_FAIL,
    GET_SINGLE_WORKER_REQUEST_SUCCESS,
    UPDATE_WORKER_REQUEST,
    UPDATE_WORKER_REQUEST_SUCCESS,
    UPDATE_WORKER_REQUEST_FAIL,
    singleWorker
} from "../action/workers";
import Api from "../../helpers/Api";

export default function* watcher() {
    yield takeLatest(WORkERS_LIST_REQUEST, handleWorkersList)
    yield takeLatest(WORkERS_DELETE_REQUEST, handleWorkerDelete)
    yield takeLatest(GET_SINGLE_WORKER_REQUEST, handleSingleWorker)
    yield takeLatest(UPDATE_WORKER_REQUEST, updateWorker)
}

function* handleWorkersList(action) {
    try {
        const {data} = yield call(Api.workersList)

        yield put({
            type: WORkERS_LIST_REQUEST_SUCCESS,
            payload: {
                data
            }
        })
    } catch (e) {
        yield put({
            type: WORkERS_LIST_REQUEST_FAIL,
            payload: {
                massage: 'error'
            }
        })
    }
}

function* handleWorkerDelete(action) {
    try {
        const {id} = action.payload
        const {data} = yield call(Api.workerDelete, id)

        yield put({
            type: WORkERS_DELETE_REQUEST_SUCCESS,
            payload: {
                data
            }
        })

        yield put(workersList())

    } catch (e) {
        yield put({
            type: WORkERS_DELETE_REQUEST_FAIL,
            payload: {
                massage: 'error'
            }
        })
    }
}

function* handleSingleWorker(action) {
    try {
        const {id} = action.payload
        const {data} = yield call(Api.getWorker, id)
        yield put({
            type: GET_SINGLE_WORKER_REQUEST_SUCCESS,
            payload: {
                data
            }
        })
        if (action.payload.cb && data) {
            action.payload.cb(null, data.worker[0])
        }
    } catch (e) {
        yield put({
            type: GET_SINGLE_WORKER_REQUEST_FAIL,
            payload: {
                massage: 'error'
            }
        })
    }
}

function* updateWorker(action) {
    try {
        const {formData,id} = action.payload
        const {data} = yield call(Api.updateWorker, formData, id)

        yield put({
            type: UPDATE_WORKER_REQUEST_SUCCESS,
            payload: {
                data
            }
        })
        console.log(data)

        yield put(workersList())

        if (action.payload.cb && data) {
            action.payload.cb(null, data)
        }

    } catch (e) {
        yield put({
            type: UPDATE_WORKER_REQUEST_FAIL,
            payload: {
                massage: 'error'
            }
        })
    }
}