import {call, put, takeLatest} from "redux-saga/effects";
import {REGISTER_REQUEST, REGISTER_REQUEST_FAIL, REGISTER_REQUEST_SUCCESS} from "../action/register";
import Api from "../../helpers/Api";

export default function* watcher() {
    yield takeLatest(REGISTER_REQUEST, handleRegister)
}

function* handleRegister(action) {
    try {
        const {formData} = action.payload
        const {data} = yield call(Api.registerEmployer, formData)

        yield put({
            type: REGISTER_REQUEST_SUCCESS,
            payload: {
                data
            }
        })

        if (action.payload.cb) {
            action.payload.cb(null, data)
        }

    } catch (e) {
        if (action.payload.cb) {
            action.payload.cb(e, null)
        }
        yield put({
            type: REGISTER_REQUEST_FAIL,
            payload:{
                message: e.response.data.errors.exist
            }
        })
    }
}