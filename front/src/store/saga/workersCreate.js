import {call, put, takeLatest} from "redux-saga/effects";
import {WORkERS_CREATE_REQUEST,WORkERS_CREATE_REQUEST_SUCCESS,WORkERS_CREATE_REQUEST_FAIL} from "../action/workers";
import Api from "../../helpers/Api";

export default function* watcher() {
    yield takeLatest(WORkERS_CREATE_REQUEST, handleWorkersCreate)
}

function* handleWorkersCreate(action) {
    try {
        const {formData} = action.payload
        const {data} = yield call(Api.workersCreate, formData);
        yield put({
            type: WORkERS_CREATE_REQUEST_SUCCESS,
            payload: {
                data
            }
        })

        if (action.payload.cb) {
            action.payload.cb(null, data)
        }
    } catch (e) {

        yield put({
            type: WORkERS_CREATE_REQUEST_FAIL,
            payload: {
                massage: 'error'
            }
        })
    }
}