import {call, put, takeLatest} from "redux-saga/effects";
import Api from "../../helpers/Api";
import {
    PROJECT_LIST_REQUEST,
    projectList,
    PROJECT_CREATE_REQUEST,
    PROJECT_CREATE_REQUEST_SUCCESS,
    PROJECT_CREATE_REQUEST_FAIL,
    GET_SINGLE_PROJECT_REQUEST_FAIL,
    GET_SINGLE_PROJECT_REQUEST,
    GET_SINGLE_PROJECT_REQUEST_SUCCESS,
    PROJECT_LIST_REQUEST_SUCCESS,
    PROJECT_LIST_REQUEST_FAIL, UPDATE_PROJECT_REQUEST, UPDATE_PROJECT_REQUEST_SUCCESS, UPDATE_PROJECT_REQUEST_FAIL
} from "../action/projectAction";
import {PROJECT_DELETE_REQUEST, PROJECT_DELETE_FAIL, PROJECT_DELETE_SUCCESS} from "../action/projectAction";
import {
    GET_SINGLE_WORKER_REQUEST_FAIL,
    GET_SINGLE_WORKER_REQUEST_SUCCESS, UPDATE_WORKER_REQUEST_FAIL,
    UPDATE_WORKER_REQUEST_SUCCESS, workersList
} from "../action/workers";

export default function* watcher() {
    yield takeLatest(PROJECT_LIST_REQUEST, handleProjectList)
    yield takeLatest(PROJECT_DELETE_REQUEST, handleProjectDelete)
    yield takeLatest(PROJECT_CREATE_REQUEST, handleProjectCreate)
    yield takeLatest(GET_SINGLE_PROJECT_REQUEST, handleSingleProject)
    yield takeLatest(UPDATE_PROJECT_REQUEST, updateProject)
}

function* handleProjectList(action) {
    try {
        const {data} = yield call(Api.getProjects)
        yield put({
            type: PROJECT_LIST_REQUEST_SUCCESS,
            payload: {
                data
            }
        })
    } catch (e) {

        yield put({
            type: PROJECT_LIST_REQUEST_FAIL,
            payload: {
                message: e.message
            }
        })
    }
}

function* handleProjectDelete(action) {
    try {
        const {id} = action.payload
        const {data} = yield call(Api.projectDelete, id)
        yield put({
            type: PROJECT_DELETE_SUCCESS,
            payload: {
                data
            }
        })
        yield put(projectList())
    } catch (e) {

        yield put({
            type: PROJECT_DELETE_FAIL,
            payload: {
                message: e.message
            }
        })
    }
}

function* handleProjectCreate(action) {
    try {
        const {formData} = action.payload
        console.log(formData)
        const {data} = yield call(Api.projectCreate, formData)
        console.log(data)
        yield put({
            type: PROJECT_CREATE_REQUEST_SUCCESS,
            payload: {
                data
            }
        })
        yield put(projectList())

        if (action.payload.cb) {
            action.payload.cb(null, data)
        }
    } catch (e) {

        yield put({
            type: PROJECT_CREATE_REQUEST_FAIL,
            payload: {
                message: e.message
            }
        })
    }
}

function* handleSingleProject(action) {
    try {
        const {id} = action.payload

        const {data} = yield call(Api.getSingleProject, id)
        yield put({
            type: GET_SINGLE_WORKER_REQUEST_SUCCESS,
            payload: {
                data
            }
        })
        if (action.payload.cb && data) {
            action.payload.cb(null, data)
        }
    } catch (e) {
        yield put({
            type: GET_SINGLE_WORKER_REQUEST_FAIL,
            payload: {
                massage: 'error'
            }
        })
    }
}
function* updateProject(action) {
    try {
        const {formData,id} = action.payload
        const {data} = yield call(Api.updateProject, formData, id)

        yield put({
            type: UPDATE_PROJECT_REQUEST_SUCCESS,
            payload: {
                data
            }
        })

        yield put(projectList())

        if (action.payload.cb && data) {
            action.payload.cb(null, data)
        }

    } catch (e) {
        yield put({
            type: UPDATE_PROJECT_REQUEST_FAIL,
            payload: {
                massage: 'error'
            }
        })
    }
}