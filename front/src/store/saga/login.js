import {call, put, takeLatest} from "redux-saga/effects";
import Api from "../../helpers/Api";
import {LOGIN_REQUEST, LOGIN_REQUEST_FAIL, LOGIN_REQUEST_SUCCESS} from "../action/login";

export default function* watcher() {
    yield takeLatest(LOGIN_REQUEST, handleLogin)
}

function* handleLogin(action) {
    try {
        console.log(1111)
        const {formData} = action.payload
        console.log(formData)
        const {data} = yield call(Api.LoginEmployer, formData)
        console.log(data)
        yield put({
            type: LOGIN_REQUEST_SUCCESS,
            payload: {
                data
            }
        })

        if (action.payload.cb) {
            action.payload.cb(null, data)
        }

    } catch (e) {
        console.log(e)
        if (action.payload.cb) {
            action.payload.cb(e.response.data.errors, null)
        }
        yield put({
            type: LOGIN_REQUEST_FAIL,
            payload:{
                message: e.message
            }
        })
    }
}