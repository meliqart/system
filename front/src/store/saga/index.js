import {all, fork} from 'redux-saga/effects'
import register from "./register";
import login from "./login";
import workers from "./workers";
import workersCreate from './workersCreate'
import employer from './employer'
import project from './projectSaga'

export default function* watchers() {
    yield all([
        register,
        employer,
        login,
        workersCreate,
        workers,
        project
    ].map(fork));
}