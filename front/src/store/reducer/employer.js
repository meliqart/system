import {EMPLOYER_REQUEST,EMPLOYER_REQUEST_FAIL,EMPLOYER_REQUEST_SUCCESS} from "../action/employer";


const initialState = {
    data: [],
    token: '',
    dataStatus: '',
    errors: '',
}

export default function reducer(state = initialState, action) {
    switch (action.type) {
        case EMPLOYER_REQUEST: {
            return {
                ...state,
                dataStatus: 'request'
            }
        }

        case EMPLOYER_REQUEST_SUCCESS: {
            return {
                ...state,
                data: action.payload.data,
                dataStatus: 'success'
            }
        }

        case EMPLOYER_REQUEST_FAIL: {
            return {
                ...state,
                dataStatus: 'fail',
                errors: action.payload.message
            }
        }

        default: {
            return {
                ...state,
            }
        }
    }
}