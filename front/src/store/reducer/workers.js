import {WORkERS_LIST_REQUEST, WORkERS_LIST_REQUEST_SUCCESS, WORkERS_LIST_REQUEST_FAIL} from "../action/workers";


const initialState = {
    data: [],
    dataStatus: '',
    errors: '',
}

export default function reducer(state = initialState, action) {
    switch (action.type) {
        case WORkERS_LIST_REQUEST: {
            return {
                ...state,
                dataStatus: 'request'
            }
        }

        case WORkERS_LIST_REQUEST_SUCCESS: {
            return {
                ...state,
                data: action.payload.data.workers,
                dataStatus: 'success'
            }
        }

        case WORkERS_LIST_REQUEST_FAIL: {
            return {
                ...state,
                dataStatus: 'fail',
            }
        }

        default: {
            return {
                ...state,
            }
        }
    }
}