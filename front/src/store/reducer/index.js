import {combineReducers} from "redux";
import register from './register'
import login from "./login";
import workers from "./workers";
import workersCreate from "./workersCreate";
import employer from "./employer";
import project from "./projectReducer";

export default combineReducers({
        register,
        employer,
        workers,
        login,
        workersCreate,
        project
})