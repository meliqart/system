import {LOGIN_REQUEST, LOGIN_REQUEST_FAIL, LOGIN_REQUEST_SUCCESS} from "../action/login";


const initialState = {
    data: [],
    token: '',
    dataStatus: '',
    errors: '',
}

export default function reducer(state = initialState, action) {
    switch (action.type) {
        case LOGIN_REQUEST: {
            return {
                ...state,
                dataStatus: 'request'
            }
        }

        case LOGIN_REQUEST_SUCCESS: {
            const token = action.payload.data.token || ""
            return {
                ...state,
                data: action.payload.data,
                token,
                dataStatus: 'success'
            }
        }

        case LOGIN_REQUEST_FAIL: {
            return {
                ...state,
                dataStatus: 'fail',
                errors: action.payload.message
            }
        }

        default: {
            return {
                ...state,
            }
        }
    }
}