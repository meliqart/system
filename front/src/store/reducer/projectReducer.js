import {PROJECT_LIST_REQUEST,PROJECT_LIST_REQUEST_SUCCESS,PROJECT_LIST_REQUEST_FAIL} from "../action/projectAction";


const initialState = {
    data: [],
    token: '',
    dataStatus: '',
    errors: '',
}

export default function reducer(state = initialState, action) {
    switch (action.type) {
        case PROJECT_LIST_REQUEST: {
            return {
                ...state,
                dataStatus: 'request'
            }
        }

        case PROJECT_LIST_REQUEST_SUCCESS: {
            return {
                ...state,
                data: action.payload.data,
                dataStatus: 'success'
            }
        }

        case PROJECT_LIST_REQUEST_FAIL: {
            return {
                ...state,
                dataStatus: 'fail',
                errors: action.payload.message
            }
        }

        default: {
            return {
                ...state,
            }
        }
    }
}