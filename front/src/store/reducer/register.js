import {REGISTER_REQUEST, REGISTER_REQUEST_FAIL, REGISTER_REQUEST_SUCCESS} from "../action/register";


const initialState = {
    data: [],
    dataStatus:'',
    errors:{},
}


export default function reducer(state = initialState, action) {
    switch (action.type) {
        case REGISTER_REQUEST: {
            return {
                ...state,
                dataStatus: 'request'
            }
        }

        case REGISTER_REQUEST_SUCCESS: {
            return {
                ...state,
                data: action.payload.data,
                dataStatus: 'success'
            }
        }

        case REGISTER_REQUEST_FAIL: {
            return {
                ...state,
                dataStatus: 'fail',
                errors: action.payload.message
            }
        }

        default: {
            return {
                ...state,
            }
        }
    }
}
