import {WORkERS_CREATE_REQUEST,WORkERS_CREATE_REQUEST_SUCCESS,WORkERS_CREATE_REQUEST_FAIL} from "../action/workers";


const initialState = {
    data: [],
    dataStatus: '',
    errors: '',
}

export default function reducer(state = initialState, action) {
    switch (action.type) {
        case WORkERS_CREATE_REQUEST: {
            return {
                ...state,
                dataStatus: 'request'
            }
        }

        case WORkERS_CREATE_REQUEST_SUCCESS: {
            return {
                ...state,
                data: action.payload.data.workers,
                dataStatus: 'success'
            }
        }

        case WORkERS_CREATE_REQUEST_FAIL: {
            return {
                ...state,
                dataStatus: 'fail',
            }
        }

        default: {
            return {
                ...state,
            }
        }
    }
}