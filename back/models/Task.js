import {DataTypes, Model} from "sequelize";
import db from "../services/db";
import Employers from "./Employers";
import Workers from "./Workers";
import Projects from "./Projects";


class Task extends Model {

}

Task.init({
    id: {
        type: DataTypes.BIGINT.UNSIGNED,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false
    },
    taskName: {
        type: DataTypes.STRING,
        allowNull: false
    },
    description: {
        type: DataTypes.TEXT,
        allowNull: false,
    },
    status: {
        type:DataTypes.STRING,
        allowNull:false,
        defaultValue:'new created'
    }

}, {
    sequelize: db,
    modelName: 'Tasks',
    tableName: 'task'
})

Task.belongsTo(Workers, {
    foreignKey: 'workerId',
    onDelete: 'cascade',
    onUpdate: 'cascade'
})


Workers.hasMany(Task,{
    foreignKey: 'workerId',
    onDelete: 'cascade',
    onUpdate: 'cascade'
})

Task.belongsTo(Projects, {
    foreignKey: 'projectId',
    onDelete: 'cascade',
    onUpdate: 'cascade'
})
Projects.hasMany(Task,{
    foreignKey: 'projectId',
    onDelete: 'cascade',
    onUpdate: 'cascade',
})

Task.belongsTo(Employers, {
    foreignKey: 'employerId',
    onDelete: 'cascade',
    onUpdate: 'cascade'
})
Employers.hasMany(Task, {
    foreignKey: 'employerId',
    onDelete: 'cascade',
    onUpdate: 'cascade'
})

export default Task