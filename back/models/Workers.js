import {DataTypes, Model} from 'sequelize';
import db from '../services/db';
import Employers from "./Employers";

class Workers extends Model {

}


Workers.init({
    id: {
        type: DataTypes.BIGINT.UNSIGNED,
        autoIncrement: true,
        primaryKey: true,
    },
    name: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    surName: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    email: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: 'email',
    }

}, {
    modelName: 'workers',
    tableName: 'Workers',
    sequelize: db,
    timestamps: false,
});

Workers.belongsTo(Employers, {
    as:'employer',
    foreignKey: 'employerId',
    onDelete: 'cascade',
    onUpdate: 'cascade'
})
Employers.hasMany(Workers, {
    as:'worker',
    foreignKey: 'employerId',
    onDelete: 'cascade',
    onUpdate: 'cascade'
})


export default Workers;
