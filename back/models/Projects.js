import {DataTypes, Model} from "sequelize";
import db from "../services/db";
import Employers from "./Employers";


class Projects extends Model {

}

Projects.init({
    id: {
        type: DataTypes.BIGINT.UNSIGNED,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false
    },
    projectName: {
        type: DataTypes.STRING,
        allowNull: false
    },
    description: {
        type: DataTypes.TEXT,
        allowNull: false,
    },

}, {
    sequelize: db,
    modelName: 'Projects',
    tableName: 'projects'
})

Projects.belongsTo(Employers, {
    as:'employer',
    foreignKey: 'employerId',
    onDelete: 'cascade',
    onUpdate: 'cascade'
})
Employers.hasMany(Projects, {
    as:'project',
    foreignKey: 'employerId',
    onDelete: 'cascade',
    onUpdate: 'cascade'
})

export default Projects