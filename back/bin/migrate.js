import Employers from "../models/Employers";
import Workers from "../models/Workers";
import Projects from "../models/Projects";
import Task from "../models/Task";


async function main() {
    try {
        for (const Model of [
            Employers,
            Workers,
            Projects,
            Task
        ]) {
            await Model.sync({alter: true});
        }
    } catch (e) {
        console.log(e);
    }

    process.exit(0);
}

main();
