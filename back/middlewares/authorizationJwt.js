import HttpErrors from "http-errors";
import jwt from "jsonwebtoken";

const EXCLUDE = [
    '/employer/register',
    '/employer/login',
]

export default function authorizationJwt(req, res, next) {
    try {
        const {originalUrl, method} = req;
        const {authorization = ''} = req.headers;
        const {JWT_SECRET} = process.env;
        console.log(originalUrl)

        if (method === 'OPTIONS' || EXCLUDE.includes(originalUrl.replace(/\?.*/, ''))) {
            next();
            return;
        }

        let id
        try {
            const data = jwt.verify(authorization.replace('Bearer ', ''), JWT_SECRET);
            id = data.id;
        } catch (e) {

        }
        if (!id) {
            throw HttpErrors(401);
        }
        req.employerId = id;
        next();
    } catch (e) {
        next(e);
    }
}
