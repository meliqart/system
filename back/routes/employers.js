import express from "express";
import EmployersController from "../controllers/EmployersController";
import authorizationJwt from "../middlewares/authorizationJwt";


const router = express.Router();


router.post('/register', EmployersController.register);
router.post('/login', EmployersController.logIn);
router.get('/get/data', EmployersController.employer);
router.delete('/delete', EmployersController.deleteEmployer);
router.put('/update', EmployersController.updateEmployer);

export default router
