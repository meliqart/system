import express from "express";
import employer from "./employers";
import workers from "./workers";
import projects from "./projects";
import tasks from "./tasks";

const router = express.Router();

router.use('/employer', employer)
router.use('/workers', workers)
router.use('/projects', projects)
router.use('/tasks', tasks)

export default router
