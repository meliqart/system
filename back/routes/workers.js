import express from "express";
import WorkersController from "../controllers/WorkersController";
const router = express.Router();

router.post('/register', WorkersController.register);
router.post('/login', WorkersController.logIn);

router.post('/create',WorkersController.create)
router.get('/list', WorkersController.workersList);
router.get('/single/:workerId', WorkersController.singleWorker);
router.delete('/delete/:workerId', WorkersController.deleteWorkers);
router.put('/update/:workerId', WorkersController.updateWorkers);

export default router
