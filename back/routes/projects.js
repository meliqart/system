import express from "express";
import ProjectsController from "../controllers/ProjectsController";
const router = express.Router();


router.post('/create',ProjectsController.createProject)
router.delete('/delete/:projectId', ProjectsController.deleteProject);
router.put('/update/:projectId', ProjectsController.updateProject);
router.get('/list', ProjectsController.projectList);
router.get('/single/:projectId', ProjectsController.singleProject);


export default router
