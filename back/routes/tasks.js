import express from "express";
import TasksController from "../controllers/TasksController";
const router = express.Router();


router.post('/create',TasksController.createTask)
router.delete('/delete/:taskId', TasksController.deleteTask);
router.put('/update/:taskId', TasksController.updateTask);
router.get('/single/:taskId', TasksController.singleTask);
router.get('/list', TasksController.taskList);
router.get('/list/:workerOrProjectId', TasksController.workerOrProjectTaskList);


export default router
