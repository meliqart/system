import {Employers} from "../models";
import HttpError from "http-errors";
import validate from "../services/validate";
import md5 from "md5";
import jwt from "jsonwebtoken";

const {JWT_SECRET, PASSWORD_SECRET} = process.env;

class EmployersController {
    static register = async (req, res, next) => {
        try {
            console.log(req.body)
            validate(req.body, {
                name: 'required|alpha',
                surName: 'required|alpha',
                email: 'required|email',
                password: 'min:8',
                phone: 'required|numeric'
            });
            const {
                name, surName, email, password, phone,
            } = req.body;

            const exist = await Employers.findOne({
                where: {
                    email,
                },
            });
            const exist1 = await Employers.findOne({
                where: {
                    phone,
                },
            });
            if (exist) {
                throw HttpError(403, {
                    errors: {
                        status: 'email',
                        exist: 'this email is ready',
                    },
                });
            }
            if (exist1) {
                throw HttpError(403, {
                    errors: {
                        status: 'phone',
                        exist: 'this  phone is ready',
                    },
                });
            }
            await Employers.create({
                name, surName, email, password, phone
            });
            res.json({
                status: 'Registered',
            });

        } catch (e) {
            next(e)
        }
    }
    static logIn = async (req, res, next) => {
        try {
            validate(req.body, {
                email: 'required|email',
                password: 'required|min:8',
            });
            const {email, password} = req.body;
            const employer = await Employers.findOne({
                where: {
                    email,
                },
            });
            if (!employer) {
                throw HttpError(422, {
                    errors: {email: ['email does not exist']}
                })
            }
            if (md5(md5(password) + PASSWORD_SECRET) !== employer.getDataValue('password')) {
                throw HttpError(422, {
                    errors: {
                        password: ['the password is incorrect'],
                    },
                });
            }

            const token = jwt.sign({id: employer.id}, JWT_SECRET);

            res.json({
                status: 'ok',
                employer,
                token,
            });


        } catch (e) {
            next(e)
        }
    }

    static deleteEmployer = async (req, res, next) => {
        try {
            const {employerId} = req;
            if (!employerId) {
                throw HttpError(422, {
                    errors: {
                        hotelId: ['employer id is not found'],
                    },
                })
            }
            await Employers.destroy({
                where: {
                    id: employerId
                }
            })
            res.json({
                status: "employer deleted"
            })


        } catch (e) {
            next(e)
        }
    }
    static updateEmployer = async (req, res, next) => {
        try {
            validate(req.body, {
                name: 'alpha',
                surName: 'alpha',
                password: 'min:8',
            });
            const {
                name, surName, password, newPassword
            } = req.body;

            const {employerId} = req;

            const employer = await Employers.findByPk(employerId)

            const employerUpdate = {
                name: name || employer.name,
                surName: surName || employer.surName,
            };
            if (password && newPassword) {
                if (md5(md5(password) + PASSWORD_SECRET) === employer.getDataValue('password')) {
                    employerUpdate.password = newPassword;
                } else {
                    throw new HttpError(422, {
                        errors: {
                            password: ['invalid password'],
                        },
                    });
                }
            }
            const [update] = await Employers.update(employerUpdate, {where: {id: employerId}});
            res.json({
                status: 'ok',
                update,
            });


        } catch (e) {
            next(e)
        }
    }
    static employer = async (req, res, next) => {
        try {
            const {employerId} = req;
            console.log(employerId)

            const employer = await Employers.findByPk(employerId);
            res.json({
                status: 'ok',
                employer
            })
        } catch (e) {
            next(e)
        }
    }

}

export default EmployersController