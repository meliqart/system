import validate from "../services/validate";
import {Employers, Workers} from "../models";
import HttpError from "http-errors";

class WorkersController {
    static register = (req, res, next) => {
        try {

        } catch (e) {
            next(e)
        }
    }
    static logIn = (req, res, next) => {
        try {

        } catch (e) {
            next(e)
        }
    }
    static create = async (req, res, next) => {
        try {
            validate(req.body, {
                name: 'required|alpha',
                surName: 'required|alpha',
                email: 'required|email',
            });
            const {name, surName, email} = req.body;
            const {employerId} = req;

            const employer = await Employers.findByPk(employerId);
            const {id} = employer
            const worker = await Workers.create({name, surName, email, employerId: id})

            res.json({
                status: "ok",
                worker
            })

        } catch (e) {
            next(e)
        }
    }

    static deleteWorkers = async (req, res, next) => {
        try {
            const {workerId} = req.params;
            if (!workerId) {
                throw HttpError(422, {
                    errors: {
                        workerId: ['worker id is not found'],
                    },
                })
            };
            const worker = await Workers.findByPk(workerId);

            if (!worker){
                throw HttpError(422, {
                    errors: {
                        workerId: ['worker not found'],
                    },
                })
            }
            const deletedWorker = await Workers.destroy({
                where: {
                    id: workerId
                }
            });
            res.json({
                status: "worker deleted",
                deletedWorker,
                worker
            })

        } catch (e) {
            next(e)
        }
    }
    static updateWorkers = async (req, res, next) => {
        try {
            validate(req.body, {
                name: 'required|alpha',
                surName: 'required|alpha',
            });

            const {workerId} = req.params;
            const {name, surName} = req.body;

            if (!workerId) {
                throw HttpError(422, {
                    errors: {
                        hotelId: ['workerId id is not found'],
                    },
                })
            }
            const worker = await Workers.findByPk(workerId)

            const workerUpdate = {
                name: name || worker.name,
                surName: surName || worker.surName,
            };

            const [update] = await Workers.update(workerUpdate, {where: {id: workerId}});

            res.json({
                status: "worker updated",
                update
            })


        } catch (e) {
            next(e)
        }
    }
    static workersList = async (req, res, next) => {
        try {
            const {employerId} = req;

            const workers = await Workers.findAll({
                where: {
                    employerId
                }
            })

            res.json({
                status: "ok",
                workers
            })

        } catch (e) {
            next(e)
        }
    }
    static singleWorker = async (req, res, next) => {
        try {
            const {workerId} = req.params;

            const worker = await Workers.findAll({
                where: {
                    id: workerId
                }
            })

            res.json({
                status: "ok",
                worker
            })

        } catch (e) {
            next(e)
        }
    }

}

export default WorkersController