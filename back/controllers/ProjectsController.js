import validate from "../services/validate";
import {Employers, Projects, Workers} from "../models";
import HttpError from "http-errors";

class ProjectsController {
    static createProject = async (req, res, next) => {
        try {
            validate(req.body, {
                projectName: 'required|alpha',
                description: 'required',
            });
            const {projectName, description} = req.body;
            const {employerId} = req;
            const employer = await Employers.findByPk(employerId)
            const {id} = employer
            const project = await Projects.create({projectName, description, employerId: id})
            res.json({
                status: "ok",
                project
            })

        } catch (e) {
            next(e)
        }
    }

    static deleteProject = async (req, res, next) => {
        try {
            const {projectId} = req.params;

            if (!projectId) {
                throw HttpError(422, {
                    errors: {
                        hotelId: ['project id is not found'],
                    },
                })
            }
            const project = await Projects.findByPk(projectId);
            if (!project) {
                throw HttpError(422, {
                    errors: {
                        hotelId: ['project not found'],
                    },
                })
            }

            const deletedProject = await Projects.destroy({
                where: {
                    id: projectId
                }
            })
            res.json({
                status: "project deleted",
                deletedProject
            })

        } catch (e) {
            next(e)
        }
    }
    static updateProject = async (req, res, next) => {
        try {
            validate(req.body, {
                projectName: 'required|alpha',
                description: 'required',
            });
            const {projectId} = req.params;
            const {projectName, description} = req.body;

            const project = await Projects.findByPk(projectId)

            const projectUpdate = {
                projectName: projectName || project.projectName,
                description: description || project.description
            }
            const [update] = await Projects.update(projectUpdate, {
                where: {
                    id: projectId
                }
            })

            res.json({
                status: "project updated",
                update
            })

        } catch (e) {
            next(e)
        }
    }
    static projectList = async (req, res, next) => {
        try {
            const {employerId} = req;

            const projects = await Projects.findAll({
                where: {
                    employerId
                }
            })

            res.json({
                status: "ok",
                projects
            })

        } catch (e) {
            next(e)
        }
    }
    static singleProject = async (req, res, next) => {
        try {
            const {projectId} = req.params;
            const project = await Projects.findByPk(projectId)

            res.json({
                status: "ok",
                project
            })

        } catch (e) {
            next(e)
        }
    }

}

export default ProjectsController