import validate from "../services/validate";
import {Employers, Projects, Task, Workers} from "../models";
import HttpError from "http-errors";

class TasksController {
    static createTask = async (req, res, next) => {
        try {
            validate(req.body, {
                taskName: 'required|alpha',
                description: 'required',
            });
            const {taskName, description, status, projectId, workerId} = req.body;

            const {employerId} = req;

            const employer = await Employers.findByPk(employerId);
            const {id} = employer

            if (projectId) {
                const existsProject = await Projects.findByPk(projectId)
                if (!existsProject) {
                    throw HttpError(422, {
                        errors: {
                            workerId: ['project not found'],
                        },
                    })
                }
            }
            if (projectId) {
                const existsWorker = await Workers.findByPk(projectId)
                if (!existsWorker) {
                    throw HttpError(422, {
                        errors: {
                            workerId: ['worker not found'],
                        },
                    })
                }
            }
            const task = await Task.create({
                taskName, description, status, projectId, workerId, employerId: id
            })
            res.json({
                status: "ok",
                task
            })

        } catch (e) {
            next(e)
        }
    }

    static deleteTask = async (req, res, next) => {
        try {
            const {taskId} = req.params;
            if (!taskId) {
                throw HttpError(422, {
                    errors: {
                        hotelId: ['task id is not found'],
                    },
                })
            }
            const task = await Task.findByPk(taskId);

            if (!task) {
                throw HttpError(422, {
                    errors: {
                        hotelId: ['task id is not found'],
                    },
                })
            }
            const deletedTask = await Task.destroy({
                where: {
                    id: taskId
                }
            })

            res.json({
                status: 'deleted',
                deletedTask
            })

        } catch (e) {
            next(e)
        }
    }
    static updateTask = async (req, res, next) => {
        try {
            validate(req.body, {
                taskName: 'required|alpha',
                description: 'required',
            });
            const {taskName, description, status, projectId, workerId} = req.body;
            const {taskId} = req.params;

            const task = await Task.findByPk(taskId);


            const taskUpdate = {
                taskName: taskName || task.taskName,
                description: description || task.description,
                status: status || task.status,
                projectId: projectId || task.projectId,
                workerId: workerId || task.workerId,
            }

            const [update] = await Task.update(taskUpdate, {
                where: {
                    id: taskId
                }
            })
            res.json({
                status:'task updated',
                update
            })

        } catch (e) {
            next(e)
        }
    }
    static singleTask = async (req, res, next) => {
        try {
            const {taskId} = req.params;

            if (!taskId) {
                throw HttpError(422, {
                    errors: {
                        hotelId: ['task id is not found'],
                    },
                })
            }

            const task = await Task.findByPk(taskId);
            if (!task) {
                throw HttpError(422, {
                    errors: {
                        hotelId: ['task is not found'],
                    },
                })
            }
            res.json({
                status: "single task",
                task
            })

        } catch (e) {
            next(e)
        }
    }


    static taskList = async (req, res, next) => {
        try {
            const {employerId} = req;

            const tasks = await Task.findAll({
                where: {
                    employerId
                }
            })
            res.json({
                status: "ok",
                tasks
            })

        } catch (e) {
            next(e)
        }
    }
    static workerOrProjectTaskList = async (req, res, next) => {
        try {
            const {employerId} = req;
            const {workerOrProjectId} = req.params

            const tasks = await Task.findAll({
                where: {
                    $or:[{projectId: workerOrProjectId},{workerId:workerOrProjectId}],
                    employerId
                }
            })
            res.json({
                status: "ok",
                tasks
            })

        } catch (e) {
            next(e)
        }
    }

}

export default TasksController